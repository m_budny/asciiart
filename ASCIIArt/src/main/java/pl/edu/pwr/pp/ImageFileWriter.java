package pl.edu.pwr.pp;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class ImageFileWriter {

	public void saveToTxtFile(char[][] ascii, String fileName) {
		// np. korzystając z java.io.PrintWriter
		// TODO Wasz kod
		
		PrintWriter writer = null;
		try{
			writer = new PrintWriter(fileName); 
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}
		
		for(int i=0;i<ascii.length;i++)
		{
			for(int j=0;j<ascii[i].length;j++)
			{
				writer.write(ascii[i][j]);
			}
			writer.println();
		}
	
		writer.close();


		
	}
	
}
