package pl.edu.pwr.pp;

public class ImageConverter {

	/**
	 * Znaki odpowiadające kolejnym poziomom odcieni szarości - od czarnego (0)
	 * do białego (255).
	 */
	public static String INTENSITY_2_ASCII = "@%#*+=-:. ";
	

	/**
	 * Metoda zwraca znak odpowiadający danemu odcieniowi szarości. Odcienie
	 * szarości mogą przyjmować wartości z zakresu [0,255]. Zakres jest dzielony
	 * na równe przedziały, liczba przedziałów jest równa ilości znaków w
	 * {@value #INTENSITY_2_ASCII}. Zwracany znak jest znakiem dla przedziału,
	 * do którego należy zadany odcień szarości.
	 * 
	 * 
	 * @param intensity
	 *            odcień szarości w zakresie od 0 do 255
	 * @return znak odpowiadający zadanemu odcieniowi szarości
	 */
	public static char intensityToAscii(int intensity) {
		// TODO Wasz kod
		char[] c_array = INTENSITY_2_ASCII.toCharArray();
		if(intensity >= 0 && intensity <= 25) return c_array[0];
		if(intensity >= 26 && intensity <= 51) return c_array[1];
		if(intensity >= 52 && intensity <= 76) return c_array[2];
		if(intensity >= 77 && intensity <= 102) return c_array[3];
		if(intensity >= 103 && intensity <= 127) return c_array[4];
		if(intensity >= 128 && intensity <= 153) return c_array[5];
		if(intensity >= 154 && intensity <= 179) return c_array[6];
		if(intensity >= 180 && intensity <= 204) return c_array[7];
		if(intensity >= 205 && intensity <= 230) return c_array[8];
		if(intensity >= 231 && intensity <= 255) return c_array[9];
		
		return 0;
	}

	/**
	 * Metoda zwraca dwuwymiarową tablicę znaków ASCII mając dwuwymiarową
	 * tablicę odcieni szarości. Metoda iteruje po elementach tablicy odcieni
	 * szarości i dla każdego elementu wywołuje {@ref #intensityToAscii(int)}.
	 * 
	 * @param intensities
	 *            tablica odcieni szarości obrazu
	 * @return tablica znaków ASCII
	 */
	public static char[][] intensitiesToAscii(int[][] intensities) {
		// TODO Wasz kod
		char[][] ascii = new char[intensities.length][];
		for(int i=0;i<intensities.length;i++)
		{
			ascii[i] = new char[intensities[i].length];
			for(int j=0;j<intensities[i].length;j++)
			{
				ascii[i][j] = intensityToAscii(intensities[i][j]);			
			}		
		}	
		return ascii;
	}

}
